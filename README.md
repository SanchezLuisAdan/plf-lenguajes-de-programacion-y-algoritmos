# Mapa conceptual 1
## Programando ordenadores en los 80 y ahora. ¿Que ha cambiado? (2016)

```plantuml
@startmindmap
*[#orange] Programando ordenadores \nen los 80 y ahora.
	*[#pink] Sistemas antiguos
		*[#lightgreen] Eran bastante limitados
		*[#lightgreen] Se tenía que conocer la arquitectura\nde hardware de cada máquina
		*[#lightgreen] Solo usaban lenguaje de bajo nivel
			* Lenguaje ensamblador
			* Código más rápido
			* Control total del equipo

	*[#pink] Sistemas actuales
		*[#lightgreen] Son más accesibles
			* Los encuentras en muchas tiendas
		*[#lightgreen] Con el tiempo son más lentos
			* El software usa más recursos
			* Sobre carga de librerías
		*[#lightgreen] Pueden usar lenguaje de alto nivel
			* Mas lento
				* Se traduce a ensamblador
			* Sobrecarga las capas realizadas
			* No se necesita saber el hardware
			* Ejemplos
				* php
				* Java
				* Javascript
				* Python

@endmindmap
```

# Mapa conceptual 2
## Hª de los algoritmos y de los lenguajes de programación (2010)
```plantuml
@startmindmap
*[#orange] Hª de los algoritmos y de los \nlenguajes de programación (2010)
	*[#pink] Algoritmos
		*[#lightgreen] Historia
			* Son usados desde hace 3000 años
			* En el siglo XIX aparecen las primeras máquinas programables
		*[#lightgreen] Es un procedimiento
			* Ordenado
			* Finito
			* Instrucciones definidas
		*[#lightgreen] Pueden ser
			* Razonables
			* No Razonables
	
	*[#pink] Maquinas programables
		*[#lightgreen] Calculadoras
			* Mecánicas
			* No programables
		*[#lightgreen] Primer computadora
			* Maquina analítica
			* Usaba tarjetas perforadas
			* operaciones básicas
				*[#lightblue] Suma
				*[#lightblue] Resta
				*[#lightblue] Multiplicación
				*[#lightblue] División
	*[#pink] Lenguajes de programación
		*[#lightgreen] Paradigmas
			* Funcional
				*[#lightblue] Simplificación
				*[#lightblue] Aparece en los 60s LISP
				*[#lightblue] otros
					* Haskell
					* Erlang
			* Lógico
				*[#lightblue] IA
				*[#lightblue] Aparece en 1971 Prolong
			* Orientado a objetos
				*[#lightblue] Organiza el diseño en torno a datos u objetos
				*[#lightblue] Aparece en 1968 Simula
				*[#lightblue] Otros
					* Java
					* php
					* Python	
@endmindmap
```

# Mapa conceptual 3
## Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)

```plantuml
@startmindmap
*[#orange] Tendencias actuales en los lenguajes \nde Programación y Sistemas Informáticos
	*[#pink] Paradigmas 
		*[#lightgreen] Comunes
			* Funcional
				*[#lightblue] Usa funciones matemáticas
				*[#lightblue] Puede usar recursividad hasta que se cumpla una condición
				*[#lightblue] Lenguajes
					* ML
					* HOPE
					* Haskell
			* Lógico
				*[#lightblue] Uso de recursividad
				*[#lightblue] Expresiones lógicas
				*[#lightblue] No usa letras ni números
				*[#lightblue] Lenguajes
					* Prolong
					* Mercury
					* CLP
			* Estructurada
				*[#lightblue] Descompone las funciones a mas sencillas
				*[#lightblue] Ejemplos
					* Java
					* C
					* Pascal
		*[#lightgreen] Actuales
			* Programación concurrente
				*[#lightblue] Paralelismo de las tareas
				*[#lightblue] Usa políticas de sincronización
				*[#lightblue] Lenguajes
					* Java
					* Haskell
			* Programación distribuida
				*[#lightblue] Comunicación entre múltiples ordenadores
				*[#lightblue] Son más pesados
				*[#lightblue] Lenguajes
					* Ada
					* E
					* Limbo
			* Programación orientado en componentes
				*[#lightblue] Reutiliza código
				*[#lightblue] mayor nivel de abstracción
				*[#lightblue] Lenguajes
					* Pascal
					* Javascript
					* Cecil
			* Programación orientada en aspectos
				*[#lightblue] Se agregan capas segun la necesidad del programa
				*[#lightblue] Deben de ser independientes para mejor legibilidad y comodidad
			* Programación orientada agente de software
				*[#lightblue] Forma partes de la programación orientada a objetos
				*[#lightblue] Multilagentes
@endmindmap
```